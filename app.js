var express = require('express'),
	path = require('path'),
	config = require('./config/config.js'),
	knox = require('knox'),
	fs = require('fs'),
	os = require('os'),
	formidable = require('formidable'),
	gm = require('gm'),
	mongoose = require('mongoose').connect(config.dbURL);


var app = express();

app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('hogan-express'));
app.set('view engine', 'html');

app.set('host', config.host);

app.use(express.static(path.join(__dirname, 'public')));
app.set('port', process.env.PORT || 3000);

var knoxClient = knox.createClient({
	key: config.S3AccessKey,
	secret: config.S3Secret,
	bucket: config.S3Bucket//,
	//endpoint: config.S3Endpoint,
	//region: config.S3Region
});

// DEBUG connection with S3 Bucket Amazon
/*knoxClient.list({}, function(err, data){
    console.log(err, data);
});*/

var server = require('http').createServer(app);
var io = require('socket.io')(server);

require('./routes/routes.js')(express, app, formidable, fs, gm, knoxClient, mongoose, io, config);

process.on('uncaughtException', function(err) {
  console.log('Caught exception: ' + err);
});

server.listen(app.get('port'), function(){
	console.log(mongoose.connection.readyState);
	console.log("PhotoGrid running on port: " + app.get('port'));
});